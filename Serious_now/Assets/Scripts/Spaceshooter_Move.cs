﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceshooter_Move : MonoBehaviour {

	public Rigidbody2D rb;
	public float thrust;
	public float turnThrust;
	private float thrustInput;
	private float turnInput;
	public float deathForce;


	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		thrustInput = Input.GetAxis("Vertical");
		turnInput = Input.GetAxis ("Horizontal");

		transform.Rotate (Vector3.forward * turnInput * Time.deltaTime * -turnThrust);

		//Killing the player for beign out of play area
	//	if (transform.position.y > screenTop) 
		//{
			//Destroy;
		//}
	}

	void FixedUpdate()
	{
		rb.AddRelativeForce (Vector2.up * thrustInput);
		//rb.AddTorque(-turnInput);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log (col.relativeVelocity.magnitude);
		if (col.relativeVelocity.magnitude > deathForce) 
		{
			Debug.Log ("Death");
			Destroy(gameObject.gameObject);

		}
	}
}
