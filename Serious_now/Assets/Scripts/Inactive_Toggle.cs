﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inactive_Toggle : MonoBehaviour {

	public static bool disabled = false;
	public float screenTop;
	public float screenBottom;
	public float screenLeft;
	public float screenRight;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y > screenTop)
		{
			Destroy(gameObject); //Makes it go away. Its pretty simple
		}
		if (transform.position.y < screenBottom)
		{
			Destroy(gameObject);
		}
		if (transform.position.y < screenLeft)
		{
			Destroy(gameObject);
		}
		if (transform.position.y > screenRight)
		{
			Destroy(gameObject);
		}
		else
		{
			gameObject.SetActive (true);
		}
			
	}	
}
