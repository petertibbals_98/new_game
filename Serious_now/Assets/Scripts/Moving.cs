﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour {

	[SerializeField]
	public float speed = 5.0f; //I dont know why that is an issue?? -->

	private Vector2 direction;

	private Transform tf; //Variable to hold transform

	// Use this for initialization
	void Start () 
	{
		direction = Vector2.up;

		//tf = GetComponent<Transform> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		GetInput (); // So you can move and stuff
		Move (); 

	}

	public void Move ()
	{
		transform.Translate (direction*speed*Time.deltaTime);
	}

	private void GetInput() // Moving in all directions and resetting world location. 
	{
		direction = Vector2.zero;

		if (Input.GetKey (KeyCode.UpArrow)) 
		{
			direction += Vector2.up;
		}
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			direction += Vector2.left;
		}
		if (Input.GetKey(KeyCode.DownArrow))
		{
			direction += Vector2.down;
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			direction += Vector2.right;
		}
		if (Input.GetKey (KeyCode.Space)) 
		{
			Transform tf = GetComponent<Transform>(); // Get the Transform component from the object as this code is on
			tf.position = Vector3.zero; // Vector3.zero is a preset value of (0,0,0)
		}

		if (Input.GetKey (KeyCode.LeftShift))
		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			//Transform tf = GetComponent<transform>();
			//tf.position = tf.position + Vector2; //This is where the Shift move function would go if I understood it better

			//Destroy (this);  Testing things out to make sure this method of typing works

			//direction += Vector2.right * 0.1f;
			//tf.position = tf.position + (Vector3.right * 0.1f); Other various ideas I had
		}

		//if (Input.GetKeyDown (KeyCode.Escape))
			//Debug.Log("Halp");
		//	Application.Quit;
	}

}
