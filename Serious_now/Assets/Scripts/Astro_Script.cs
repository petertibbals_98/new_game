﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astro_Script : MonoBehaviour 
{
	public float maxThrust;
	public float maxTorgue;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () 
	{
		Vector2 thrust = new Vector2 (Random.Range (-maxThrust, maxThrust),Random.Range (-maxThrust, maxThrust));
		float torgue = Random.Range (-maxTorgue, maxTorgue);

		rb.AddForce (thrust);
		rb.AddTorque (torgue);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	//void OnCollisionEnter2D(Collision2D col)
	//{
	//	if (col.relativeVelocity.magnitude > deathFore) 
	//	{
			
	//	}
	//}
}
